package com.test;

import java.util.Date;

public class Receipt {
    private int netCost;
    private int status;
    private Date issueDate;

    public Receipt(int netCost, int status) {
        this.netCost = netCost;
        this.status = status;
        this.issueDate = new Date();
    }

    public int getNetCost() {
        return netCost;
    }

    public int getStatus() {
        return status;
    }

    public Date getIssueDate() {
        return issueDate;
    }
}
