package com.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String cmd = null;
        label:
        while(true) {
            try {
                System.out.println("Welcome! Please select the number for one of the options below:\n" +
                        "0. New Test Request\n" +
                        "1. View Existing Test Request\n" +
                        "2. Exit");
                    cmd = reader.readLine();
                switch (cmd) {
                    case "0":
                        System.out.println("Available Tests are:");
                        ArrayList<Test> tests = TestMaster.getInstance().requestNewTestSet();
                        for (Test test : tests) {
                            System.out.println(test.getTestType() + "    " +
                                    test.getCost() + " Toman");
                        }
                        System.out.println("Please enter the tests you want, each in one line, then enter \"submit\".");
                        TestMaster.getInstance().createNewTestRequest();
                        cmd = reader.readLine();
                        while (!cmd.equals("submit")) {
                            int status = TestMaster.getInstance().addNewTest(cmd);
                            if (status < 0) {
                                System.out.println("Invalid Test");
                            } else {
                                System.out.println("Test added successfully!");
                            }
                            System.out.println("Currently your tests are: ");
                            for(int i = 0; i < TestMaster.getInstance().getTests().size(); i++) {
                                System.out.println("> " + TestMaster.getInstance().getTests().get(i).getTestType());
                            }
                            cmd = reader.readLine();
                        }
                        ArrayList<Lab> labs = TestMaster.getInstance().submitTests();
                        System.out.println("Available labs for your tests are:");
                        for (Lab lab : labs) {
                            System.out.println(lab.getName() + "    " + lab.getAddress()
                                    + "    " + lab.getPhone());
                        }
                        ArrayList<TimeSlot> timeSlots = null;
                        while (timeSlots == null) {
                            System.out.println("Please enter your chosen lab's name.");
                            cmd = reader.readLine();
                            timeSlots = TestMaster.getInstance().selectLab(cmd);
                        }
                        System.out.println("Here are the free time slots of the lab:");
                        for (TimeSlot timeSlot : timeSlots) {
                            System.out.println(timeSlot.getId() + "    " + timeSlot.getStartTime()
                                    + "    " + timeSlot.getEndTime());
                        }
                        boolean validTimeSlot = false;
                        while (!validTimeSlot) {
                            System.out.println("Please enter the id of a convenient time slot.");
                            cmd = reader.readLine();
                            for (TimeSlot timeSlot : timeSlots) {
                                if (timeSlot.getId().equals(cmd)) {
                                    validTimeSlot = true;
                                    break;
                                }
                            }
                        }
                        TestMaster.getInstance().selectTimeSlot(cmd);
                        System.out.println("Do you want to use insurance? (y/n)");
                        cmd = reader.readLine();
                        if (cmd.equals("y")) {
                            ArrayList<InsurancePlan> plans = TestMaster.getInstance().getSupportedInsuranceList();
                            System.out.println("Here are the insurance companies that support your tests:");
                            for (InsurancePlan plan : plans) {
                                System.out.println(plan.getPlanName());
                            }
                            boolean validPlan = false;
                            while (!validPlan) {
                                System.out.println("Please enter the name of your insurance company.");
                                cmd = reader.readLine();
                                for (InsurancePlan plan : plans) {
                                    if (plan.getPlanName().equals(cmd)) {
                                        validPlan = true;
                                        break;
                                    }
                                }
                            }
                            TestMaster.getInstance().addInsuranceSupport(cmd);
                        }
                        System.out.println("Your net cost is:" + TestMaster.getInstance().submitTestRequest() + " Toman.");
                        System.out.println("Please enter your pin to pay the price through bank portal: (xxxx)");
                        cmd = reader.readLine();
                        String reqId = TestMaster.getInstance().payPrice(cmd);
                        while (reqId == null) {
                            System.out.println("Incorrect pin. Please try again.");
                            cmd = reader.readLine();
                            reqId = TestMaster.getInstance().payPrice(cmd);
                        }
                        System.out.println("The request has been submitted and your request ID is:" + reqId);
                        break;
                    case "1":
                        System.out.println("Please enter your corresponding request ID:");
                        cmd = reader.readLine();
                        TestRequest testRequest = TestMaster.getInstance().getLoggedInCustomer().getTestRequest(cmd);
                        if (testRequest == null) {
                            System.out.println("Request not found!");
                        } else {
                            System.out.println("Showing test request " + testRequest.getId() + ":");
                            System.out.println("Lab: " + testRequest.getLab());
                            System.out.println("At " + testRequest.getTimeSlot());
                            System.out.println("Insurance used: " + testRequest.getInsurance());
                            System.out.println("Tests:");
                            for (int i = 0; i < testRequest.getTests().size(); i++) {
                                System.out.println("> " + testRequest.getTests().get(i).getTestType());
                            }
                            System.out.println("Net cost: " + testRequest.getCost());
                        }
                        break;
                    case "2":
                        break label;
                }
            } catch (IOException e) {
                System.out.println("Command not parsed!");
                e.printStackTrace();
            }
        }
        System.out.println("Goodbye!");
    }
}
