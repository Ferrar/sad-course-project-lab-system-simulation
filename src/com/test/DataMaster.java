package com.test;

import java.util.ArrayList;

public class DataMaster {
    private ArrayList<Test> availableTests;
    private ArrayList<Lab> availableLabs;
    private ArrayList<TimeSlot> labTimeSlots;
    private ArrayList<Customer> customers;

    private static DataMaster instance;

    public static DataMaster getInstance() {
        if(instance == null)
        {
            instance = new DataMaster();
            instance.availableTests = new ArrayList<Test>();
            instance.availableLabs = new ArrayList<Lab>();
            instance.labTimeSlots = new ArrayList<TimeSlot>();
            instance.customers = new ArrayList<Customer>();
            instance.customers.add(TestMaster.getInstance().getLoggedInCustomer());

            instance.availableTests.add(new Test("General Blood Test", 25000));
            instance.availableTests.add(new Test("Cancer Test", 150000));
            instance.availableTests.add(new Test("Urinalysis Test", 12000));
            instance.availableTests.add(new Test("Blood Pressure Test", 20000));
            instance.availableTests.add(new Test("Sugar Test", 30000));
            instance.availableTests.add(new Test("Aids Test", 60000));

            instance.availableLabs.add(new Lab("Parsian", "Shahrak-e Shargh, No. 16",
                    "021-12345678"));
            instance.availableLabs.get(instance.availableLabs.size()-1).addAvailableTest(instance.availableTests.get(0));
            instance.availableLabs.get(instance.availableLabs.size()-1).addAvailableTest(instance.availableTests.get(1));
            instance.availableLabs.get(instance.availableLabs.size()-1).addAvailableTest(instance.availableTests.get(2));
            instance.availableLabs.get(instance.availableLabs.size()-1).addAvailableTest(instance.availableTests.get(3));
            instance.availableLabs.get(instance.availableLabs.size()-1).addAvailableTest(instance.availableTests.get(4));
            instance.availableLabs.get(instance.availableLabs.size()-1).addAvailableTest(instance.availableTests.get(5));
            instance.labTimeSlots.add(new TimeSlot("Parsian","Parsian" + "-0",
                    "Monday 14", "Monday 16"));
            instance.labTimeSlots.add(new TimeSlot("Parsian","Parsian" + "-1",
                    "Tuesday 14", "Tuesday 16"));
            instance.labTimeSlots.add(new TimeSlot("Parsian","Parsian" + "-2",
                    "Tuesday 17", "Tuesday 19"));
            instance.labTimeSlots.add(new TimeSlot("Parsian","Parsian" + "-3",
                    "Wednesday 9", "Wednesday 11"));

            instance.availableLabs.add(new Lab("Emam", "Darya blvd., No. 34",
                    "021-87654321"));
            instance.availableLabs.get(instance.availableLabs.size()-1).addAvailableTest(instance.availableTests.get(3));
            instance.availableLabs.get(instance.availableLabs.size()-1).addAvailableTest(instance.availableTests.get(4));
            instance.labTimeSlots.add(new TimeSlot("Emam","Emam" + "-0",
                    "Monday 21", "Monday 23"));
            instance.labTimeSlots.add(new TimeSlot("Emam","Emam" + "-1",
                    "Tuesday 21", "Tuesday 23"));
        }
        return instance;
    }

    public Customer getCustomer(String customerId) {
        for (int i = 0; i < customers.size(); i++) {
            if (customers.get(i).getCustomerId().equals(customerId)) {
                return customers.get(i);
            }
        }
        return null;
    }

    public Lab getLab(String labId) {
        for(int i = 0; i < instance.availableLabs.size(); i++) {
            if(instance.availableLabs.get(i).getName().equals(labId)) {
                return instance.availableLabs.get(i);
            }
        }
        return null;
    }

    public ArrayList<Test> getAvailableTests() {
        return availableTests;
    }

    public void setAvailableTests(ArrayList<Test> availableTests) {
        this.availableTests = availableTests;
    }

    public ArrayList<Lab> getAvailableLabs(ArrayList<Test> tests) {
        ArrayList<Lab> labs = new ArrayList<Lab>();
        for(int i = 0; i < availableLabs.size(); i++) {
            boolean allFound = true;
            for(int k = 0; k < tests.size(); k++) {
                boolean testFound = false;
                for(int j =0; j < availableLabs.get(i).getAvailableTests().size(); j++) {
                    if(availableLabs.get(i).getAvailableTests().get(j).getTestType().equals(tests.get(k).getTestType())) {
                        testFound = true;
                    }
                }
                if(!testFound) {
                    allFound = false;
                    break;
                }
            }
            if(allFound) {
                labs.add(availableLabs.get(i));
            }
        }
        return labs;
    }

    public void setAvailableLabs(ArrayList<Lab> availableLabs) {
        this.availableLabs = availableLabs;
    }

    public ArrayList<TimeSlot> getLabTimeSlots(String labId) {
        ArrayList<TimeSlot> timeSlots = new ArrayList<TimeSlot>();
        for(int i = 0; i < instance.labTimeSlots.size(); i++) {
            if(instance.labTimeSlots.get(i).getLabId().equals(labId)) {
                timeSlots.add(instance.labTimeSlots.get(i));
            }
        }
        return timeSlots;
    }

    public void setLabTimeSlots(ArrayList<TimeSlot> labTimeSlots) {
        this.labTimeSlots = labTimeSlots;
    }

    public ArrayList<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(ArrayList<Customer> customers) {
        this.customers = customers;
    }

    public static void setInstance(DataMaster instance) {
        DataMaster.instance = instance;
    }
}
