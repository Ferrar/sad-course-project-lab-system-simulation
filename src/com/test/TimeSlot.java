package com.test;

public class TimeSlot {
    private String labId;
    private String id;
    private String startTime;
    private String endTime;

    public TimeSlot(String labId, String id, String startTime, String endTime) {
        this.labId = labId;
        this.id = id;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public void setLabId(String labId) {
        this.labId = labId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getLabId() {
        return labId;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }
}
