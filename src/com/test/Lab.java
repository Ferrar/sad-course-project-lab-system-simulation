package com.test;

import java.util.ArrayList;

public class Lab {
    private String name;
    private String address;
    private String phone;
    private ArrayList<Test> availableTests;

    public Lab(String name, String address, String phone) {
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.availableTests = new ArrayList<Test>();
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void addAvailableTest(Test test) {
        this.availableTests.add(test);
    }

    public ArrayList<Test> getAvailableTests() {
        return availableTests;
    }

    public void setAvailableTests(ArrayList<Test> availableTests) {
        this.availableTests = availableTests;
    }

    public void notifyLab() {
        System.out.println("The lab has been sent a notification!");
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }
}
