package com.test;

import java.util.ArrayList;

public class InsurancePlan {
    private String planName;
    private double reductionFactor;
    private ArrayList<String> supportedTests;

    public InsurancePlan(String planName, double reductionFactor, ArrayList<String> supportedTests) {
        this.planName = planName;
        this.reductionFactor = reductionFactor;
        this.supportedTests = supportedTests;
    }

    public String getPlanName() {
        return planName;
    }

    public double getReductionFactor() {
        return reductionFactor;
    }

    public ArrayList<String> getSupportedTests() {
        return supportedTests;
    }
}
