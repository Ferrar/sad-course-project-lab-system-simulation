package com.test;

public class Test {
    private String testType;
    private int cost;

    public Test(String testType, int cost) {
        this.testType = testType;
        this.cost = cost;
    }

    public String getTestType() {
        return testType;
    }

    public int getCost() {
        return cost;
    }
}
