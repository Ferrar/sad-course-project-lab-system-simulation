package com.test;

import java.util.ArrayList;

public class TestRequest {
    private int cost;
    private ArrayList<Test> tests;
    private String lab;
    private String timeSlot;
    private String insurance;

    public TestRequest(String id) {
        this.id = id;
        this.cost = 0;
        this.tests = new ArrayList<Test>();
    }

    public String getTimeSlot() {
        return timeSlot;
    }

    public String getInsurance() {
        return insurance;
    }

    public void setInsuranceSupport(String insId) {
        this.insurance = insId;
    }

    public void setTimeSlot(String timeSlot) {
        this.timeSlot = timeSlot;
    }

    private String id;

    public void setCost(int cost) {
        this.cost = cost;
    }

    public ArrayList<Test> getTests() {
        return tests;
    }

    public void setTests(ArrayList<Test> tests) {
        this.tests = tests;
    }

    public String getLab() {
        return lab;
    }

    public void setLab(String lab) {
        this.lab = lab;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<Test> addNewTest(String testId) {
        Test test = null;
        for(int i = 0; i < tests.size(); i++) {
            if(tests.get(i).getTestType().equals(testId)) {
                return tests;
            }
        }
        for(int i = 0; i < DataMaster.getInstance().getAvailableTests().size(); i++) {
            if(DataMaster.getInstance().getAvailableTests().get(i).getTestType().equals(testId)) {
                test = DataMaster.getInstance().getAvailableTests().get(i);
            }
        }
        if(test == null) {
            return tests;
        }
        tests.add(test);
        cost += test.getCost();
        return tests;
    }

    public int getCost() {
        cost = 0;
        for(int i = 0; i < tests.size(); i++) {
            cost += tests.get(i).getCost();
        }
        InsurancePlan plan = InsuranceSystem.getInstance().getInsurancePlan(insurance);
        if(plan != null) {
            cost *= plan.getReductionFactor();
        }
        return cost;
    }

    public Receipt payPrice(String pin) {
        return BankPortal.getInstance().payPrice(cost, pin);
    }
}
