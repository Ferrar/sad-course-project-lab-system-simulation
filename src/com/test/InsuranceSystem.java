package com.test;

import java.util.ArrayList;

public class InsuranceSystem {
    private ArrayList<InsurancePlan> plans;

    private static InsuranceSystem instance;

    public static InsuranceSystem getInstance() {
        if(instance == null)
        {
            instance = new InsuranceSystem();
            ArrayList<String> supports = new ArrayList<>();
            instance.plans = new ArrayList<>();
            supports.add("General Blood Test");
            supports.add("Cancer Test");
            supports.add("Urinalysis Test");
            supports.add("Blood Pressure Test");
            supports.add("Sugar Test");
            supports.add("Aids Test");
            instance.plans.add(new InsurancePlan("Tamin Ejtemayi", 0.5, supports));
            supports = new ArrayList<String>();
            supports.add("General Blood Test");
            supports.add("Blood Pressure Test");
            instance.plans.add(new InsurancePlan("Sina", 0.7, supports));
        }
        return instance;
    }

    public InsuranceSystem() {
    }

    public void setPlans(ArrayList<InsurancePlan> plans) {
        this.plans = plans;
    }

    public void addPlan(InsurancePlan plan) {
        this.plans.add(plan);
    }

    public ArrayList<InsurancePlan> getAvailableInsurance(ArrayList<Test> tests) {
        ArrayList<InsurancePlan> insurancePlans = new ArrayList<InsurancePlan>();
        for(int i = 0; i < plans.size(); i++) {
            boolean allFound = true;
            for(int k = 0; k < tests.size(); k++) {
                boolean testFound = false;
                for(int j =0; j < plans.get(i).getSupportedTests().size(); j++) {
                    if(plans.get(i).getSupportedTests().get(j).equals(tests.get(k).getTestType())) {
                        testFound = true;
                    }
                }
                if(!testFound) {
                    allFound = false;
                    break;
                }
            }
            if(allFound) {
                insurancePlans.add(plans.get(i));
            }
        }
        return insurancePlans;
    }

    public InsurancePlan getInsurancePlan(String planId) {
        for(int i = 0; i < plans.size(); i++) {
            if(plans.get(i).getPlanName().equals(planId)) {
                return plans.get(i);
            }
        }
        return null;
    }
}
