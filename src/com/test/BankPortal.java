package com.test;

public class BankPortal {
    private static BankPortal instance;

    public static BankPortal getInstance() {
        if(instance == null)
        {
            instance = new BankPortal();
        }
        return instance;
    }

    public Receipt payPrice(int cost, String pin) {
        if(pin.equals("1234")) {
            return new Receipt(cost, 0);
        }
        else {
            return new Receipt(cost, -1);
        }
    }
}
