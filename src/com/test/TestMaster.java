package com.test;

import java.util.ArrayList;
import java.util.Date;

public class TestMaster {
    private static TestMaster instance;
    private Customer loggedInCustomer;
    private TestRequest newTestRequest;

    public static TestMaster getInstance() {
        if(instance == null)
        {
            instance = new TestMaster();
            instance.loggedInCustomer = new Customer("Mamali", idGenerator(), 21,
                    "Lalehzar Blvd, No. 21", "021-22334455");
        }
        return instance;
    }

    public static String idGenerator() {
        return Integer.toString(new Date().hashCode());
    }

    public ArrayList<Test> requestNewTestSet() {
        return DataMaster.getInstance().getAvailableTests();
    }

    public String createNewTestRequest() {
        String id = idGenerator();
        instance.newTestRequest = new TestRequest(id);
        return id;
    }

    public int addNewTest(String testId) {
        ArrayList<Test> tests = DataMaster.getInstance().getAvailableTests();
        for(int i = 0; i < tests.size(); i++) {
            if(tests.get(i).getTestType().equals(testId)) {
                for(int k = 0; k < instance.newTestRequest.getTests().size(); k++) {
                    if(instance.newTestRequest.getTests().get(k).getTestType().equals(testId)) {
                        return -1;
                    }
                }
                instance.newTestRequest.addNewTest(testId);
                return 0;
            }
        }
        return -1;
    }

    public ArrayList<Test> getTests() {
        if(instance.newTestRequest != null)
            return instance.newTestRequest.getTests();
        return null;
    }

    public ArrayList<Lab> submitTests() {
        return DataMaster.getInstance().getAvailableLabs(instance.newTestRequest.getTests());
    }

    public ArrayList<TimeSlot> selectLab(String labId) {
        Lab lab = DataMaster.getInstance().getLab(labId);
        if(lab == null) {
            return null;
        }
        instance.newTestRequest.setLab(labId);
        return DataMaster.getInstance().getLabTimeSlots(labId);
    }

    public void selectTimeSlot(String slotId) {
        instance.newTestRequest.setTimeSlot(slotId);
    }

    public ArrayList<InsurancePlan> getSupportedInsuranceList() {
        return InsuranceSystem.getInstance().getAvailableInsurance(instance.newTestRequest.getTests());
    }

    public void addInsuranceSupport(String insId) {
        instance.newTestRequest.setInsuranceSupport(insId);
    }

    public int submitTestRequest() {
        return instance.newTestRequest.getCost();
    }

    public String payPrice(String pin) {
        Receipt receipt = instance.newTestRequest.payPrice(pin);
        if(receipt.getStatus() < 0) {
            return null;
        }
        String requestId = instance.loggedInCustomer.addTestRequest(instance.newTestRequest);
        DataMaster.getInstance().getLab(instance.newTestRequest.getLab()).notifyLab();
        return requestId;
    }

    public Customer getLoggedInCustomer() {
        return loggedInCustomer;
    }
}
