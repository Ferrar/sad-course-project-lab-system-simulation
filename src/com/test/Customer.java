package com.test;

import java.util.ArrayList;

public class Customer {
    private String name;
    private String customerId;
    private int age;
    private String address;
    private String phone;
    private ArrayList<TestRequest> testRequests;

    public Customer(String name, String customerId, int age, String address, String phone) {
        this.name = name;
        this.customerId = customerId;
        this.age = age;
        this.address = address;
        this.phone = phone;
        this.testRequests = new ArrayList<TestRequest>();
    }

    public String addTestRequest(TestRequest testRequest) {
        testRequests.add(testRequest);
        return testRequest.getId();
    }

    public void removeTestRequest(String testReqId) {
        for(int i = 0; i < testRequests.size(); i++) {
            if(testRequests.get(i).getId().equals(testReqId)) {
                testRequests.remove(i);
                return;
            }
        }
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public ArrayList<TestRequest> getTestRequests() {
        return testRequests;
    }

    public TestRequest getTestRequest(String testReqId) {
        for(int i = 0; i < testRequests.size(); i++) {
            if(testRequests.get(i).getId().equals(testReqId)) {
                return testRequests.get(i);
            }
        }
        return null;
    }

    public void setTestRequests(ArrayList<TestRequest> testRequests) {
        this.testRequests = testRequests;
    }
}
